package ua.ithillel.evo.questengine.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;

import java.util.List;

@Repository
public interface QuestRepository extends JpaRepository<Quest, Long> {
    List<Quest> getQuestsByIsPublicTrueOrderByAccessTimeDesc();

    List<Quest> getQuestsByIsPublicTrueAndUserIdOrderByAccessTimeDesc(Long userId);

    List<Quest> getQuestsByIsPublicTrueAndTypeAndUserIdAndNameContainsIgnoreCaseOrDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
            Type type, Long userId, String name, String description
    );

    List<Quest> getQuestsByIsPublicTrueAndTypeAndUserIdOrderByAccessTimeDesc(Type type, Long userId);

    List<Quest> getQuestsByIsPublicTrueAndTypeAndNameContainsIgnoreCaseOrderByAccessTimeDesc(
            Type type, String name
    );

    List<Quest> getQuestsByIsPublicTrueAndTypeAndDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
            Type type, String description
    );

    List<Quest> getQuestsByIsPublicTrueAndTypeOrderByAccessTimeDesc(Type type);

    List<Quest> getQuestsByIsPublicTrueAndNameContainsIgnoreCaseOrDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
            String name, String description
    );
}
