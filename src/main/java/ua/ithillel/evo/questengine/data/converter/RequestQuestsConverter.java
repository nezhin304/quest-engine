package ua.ithillel.evo.questengine.data.converter;

import ua.ithillel.evo.questengine.models.RequestQuests;

import java.util.Map;

public class RequestQuestsConverter {
    public static RequestQuests convertFromMap(Map<String, String> requestsMap) {
        RequestQuests requestQuests = new RequestQuests();
        if (requestsMap.containsKey("type")) {
            requestQuests.setType(requestsMap.get("type"));
        }
        if (requestsMap.containsKey("search")) {
            requestQuests.setSearch(requestsMap.get("search"));
        }
        if (requestsMap.containsKey("author")) {
            requestQuests.setAuthor(requestsMap.get("author"));
        }
        return requestQuests;
    }
}
