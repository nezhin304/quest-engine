package ua.ithillel.evo.questengine.data.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestQuestsDto {

    private String type;

    private String search;

    private String author;
}
