package ua.ithillel.evo.questengine.data.dao.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import ua.ithillel.evo.questengine.data.dao.QuestDAO;
import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;
import ua.ithillel.evo.questengine.data.repository.QuestRepository;

import java.util.List;

@Component
public class QuestDAOImpl implements QuestDAO {

    private QuestRepository questRepository;

    @Autowired
    QuestDAOImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
    }

    @Override
    public Quest getById(Long id) {
        return questRepository.findById(id).orElse(null);
    }

    @Override
    public List<Quest> getAll() {
        return questRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
    }

    @Override
    public List<Quest> getPublic() {
        return this.questRepository.getQuestsByIsPublicTrueOrderByAccessTimeDesc();
    }

    @Override
    public List<Quest> getQuestsByUserId(Long userId) {
        return this.questRepository.getQuestsByIsPublicTrueAndUserIdOrderByAccessTimeDesc(userId);
    }

    @Override
    public List<Quest> getQuestsByTypeAndUserIdAndNameOrDescription(Type type, Long userId, String name, String description) {
        return this.questRepository
                .getQuestsByIsPublicTrueAndTypeAndUserIdAndNameContainsIgnoreCaseOrDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
                        type, userId, name, description);
    }

    @Override
    public List<Quest> getQuestsByTypeAndUserId(Type type, Long userId) {
        return this.questRepository
                .getQuestsByIsPublicTrueAndTypeAndUserIdOrderByAccessTimeDesc(type, userId);
    }

    @Override
    public List<Quest> getQuestsByTypeAndName(Type type, String name) {
        return this.questRepository.getQuestsByIsPublicTrueAndTypeAndNameContainsIgnoreCaseOrderByAccessTimeDesc(
                type, name);
    }

    @Override
    public List<Quest> getQuestsByTypeAndDescription(Type type, String description) {
        return this.questRepository.getQuestsByIsPublicTrueAndTypeAndDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
                type, description);
    }

    @Override
    public List<Quest> getQuestsByType(Type type) {
        return this.questRepository.getQuestsByIsPublicTrueAndTypeOrderByAccessTimeDesc(type);
    }

    @Override
    public List<Quest> getQuestsByNameOrDescription(String name, String description) {
        return this.questRepository
                .getQuestsByIsPublicTrueAndNameContainsIgnoreCaseOrDescriptionContainsIgnoreCaseOrderByAccessTimeDesc(
                        name, description);
    }

    @Override
    public Quest save(Quest quest) {
        return questRepository.save(quest);
    }

    @Override
    public void deleteById(Long id) {
        questRepository.deleteById(id);
    }
}
