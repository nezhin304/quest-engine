package ua.ithillel.evo.questengine.data.dao;

import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;

import java.util.List;

public interface QuestDAO {

    Quest getById(Long id);

    List<Quest> getAll();

    List<Quest> getPublic();

    List<Quest> getQuestsByUserId(Long userId);

    List<Quest> getQuestsByTypeAndUserIdAndNameOrDescription(Type type, Long userId, String name, String description);

    List<Quest> getQuestsByTypeAndUserId(Type type, Long userId);

    List<Quest> getQuestsByTypeAndName(Type type, String name);

    List<Quest> getQuestsByTypeAndDescription(Type type, String description);

    List<Quest> getQuestsByType(Type type);

    List<Quest> getQuestsByNameOrDescription(String name, String description);

    Quest save(Quest quest);

    void deleteById(Long id);
}

