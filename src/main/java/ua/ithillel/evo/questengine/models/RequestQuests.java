package ua.ithillel.evo.questengine.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestQuests {

    private String type;

    private String search;

    private String author;
}
