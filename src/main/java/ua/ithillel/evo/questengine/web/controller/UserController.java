package ua.ithillel.evo.questengine.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ua.ithillel.evo.questengine.data.converter.UserConverter;
import ua.ithillel.evo.questengine.data.dto.UserDto;
import ua.ithillel.evo.questengine.data.entity.Role;
import ua.ithillel.evo.questengine.data.entity.User;
import ua.ithillel.evo.questengine.exception.NotFoundException;
import ua.ithillel.evo.questengine.exception.UserValidationException;
import ua.ithillel.evo.questengine.service.UserService;
import ua.ithillel.evo.questengine.util.JwtUtil;
import ua.ithillel.evo.questengine.web.validation.UserValidator;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService, JwtUtil jwtUtil) {
        this.userService = userService;
    }

    //    open method OPTIONS to endpoint /users
    @RequestMapping(method = RequestMethod.OPTIONS)
    public ResponseEntity<Void> optionsToQuests(HttpServletResponse response) {
        response.setHeader("Allow", "HEAD,GET,PUT,OPTIONS");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@Valid @RequestBody UserDto userDto) throws Exception {
        User user = userService.getByEmail(userDto.getEmail());
        ObjectMapper mapper = new ObjectMapper(); // for tests
        System.out.println("userDto = " + mapper.writeValueAsString(userDto)); // for test
        System.out.println("user = " + mapper.writeValueAsString(user)); // for test
        if (user == null) {
            User newUser = UserConverter.convertFromDto(userDto);
            if (newUser.getRole() == null) {
                newUser.setRole(Role.USER);
            }
            System.out.println("newUser = " + mapper.writeValueAsString(newUser)); // for test
            UserValidator.validate(newUser);
            userService.save(newUser);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDto> getById(@PathVariable Long id) {
        User user = userService.getById(id);
        if (user != null) {
            return new ResponseEntity<>(UserConverter.convertFromEntity(user), HttpStatus.OK);
        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> currentUser(Authentication authentication) {
        return new ResponseEntity<>(
                UserConverter.convertFromEntity(userService.getByEmail(authentication.getName())),
                HttpStatus.OK
        );
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getAllUsers() {
        return new ResponseEntity<>(
                userService.getAll().stream().map(UserConverter::convertFromEntity).collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@Valid @RequestBody UserDto userDto, @PathVariable Long id) throws Exception {
        User user = userService.getById(id);
        User newUser = UserConverter.convertFromDto(userDto);
        if (user != null) {
            user.setEmail(newUser.getEmail());
            user.setPassword(newUser.getPassword());
            user.setRole(newUser.getRole());
            UserValidator.validate(user);
            userService.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        userService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
