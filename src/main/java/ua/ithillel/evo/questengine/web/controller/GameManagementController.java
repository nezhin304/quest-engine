package ua.ithillel.evo.questengine.web.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ua.ithillel.evo.questengine.data.converter.HintConverter;
import ua.ithillel.evo.questengine.data.converter.QuestionConverter;
import ua.ithillel.evo.questengine.data.dto.HintDto;
import ua.ithillel.evo.questengine.data.dto.QuestionDto;
import ua.ithillel.evo.questengine.data.entity.*;
import ua.ithillel.evo.questengine.service.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/game")
public class GameManagementController {

    private QuestService questService;
    private QuestionService questionService;
    private ProgressService progressService;
    private GameService gameService;
    private UserService userService;

    public GameManagementController(
            QuestService questService,
            QuestionService questionService,
            ProgressService progressService,
            GameService gameService,
            UserService userService
    ) {
        this.questService = questService;
        this.questionService = questionService;
        this.progressService = progressService;
        this.gameService = gameService;
        this.userService = userService;
    }

//    @PostMapping(value = "/{quest_id}", consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> checkAnswer(
//            Authentication authentication,
//            @PathVariable Long quest_id,
//            @RequestBody String answerJson
//    ) {
//
//        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
//        User currentUser = userService.getByEmail(userDetails.getUsername());
//        List<Game> allUserGames = gameService.getGamesByUserId(currentUser.getId());
//        ObjectMapper mapper = new ObjectMapper();
//        String answer = null;
//        try {
//            JsonNode jsonNodeAnswer = mapper.readTree(answerJson);
//            JsonNode idNode = jsonNodeAnswer.path("Answer");
//            answer = idNode.asText();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Game currentGame = null;
//        if (answer != null) {
//            for (Game g : allUserGames) {
//                for (Progress p : g.getProgresses()) {
//                    if (p.getQuestion().getQuest().getId().equals(quest_id)) {
//                        currentGame = g;
//                        break;
//                    }
//                }
//            }
//
//            if (currentGame != null) {
//                Progress lastProgress = currentGame.getProgresses()
//                        .stream()
//                        .filter(p -> p.getEndTime() == null)
//                        .findFirst().orElse(null);
//                if (lastProgress != null) {
//                    if (lastProgress.getStartTime() + lastProgress.getQuestion().getDuration() > System.currentTimeMillis()) {
//                        if (lastProgress.getQuestion().getAnswer().toLowerCase().equals(answer.toLowerCase())) {
//                            lastProgress.setSuccessful(true);
//                            lastProgress.setEndTime(System.currentTimeMillis());
//                            progressService.save(lastProgress);
//                            return new ResponseEntity<>(HttpStatus.OK);
//                        }
//                    } else {
//                        lastProgress.setSuccessful(false);
//                        lastProgress.setEndTime(System.currentTimeMillis());
//                        progressService.save(lastProgress);
//                        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//                    }
//                }
//            }
//        }
//        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//    }


    @PostMapping(value = "/{quest_id}/{question_number}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> checkAnswer(
            Authentication authentication,
            @PathVariable Long quest_id,
            @PathVariable Long question_number,
            @RequestBody String answerJson
    ) {
        question_number = question_number - 1;
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User currentUser = userService.getByEmail(userDetails.getUsername());
        ObjectMapper mapper = new ObjectMapper();
        Quest quest = questService.getById(quest_id);
        List<Question> questions = quest.getQuestions();
        Question question = questions.get(Math.toIntExact(question_number));
        String answer = null;
        try {
            JsonNode jsonNodeAnswer = mapper.readTree(answerJson);
            JsonNode idNode = jsonNodeAnswer.path("Answer");
            answer = idNode.asText();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (answer != null) {

            List<Progress> progressList = question.getProgresses();
            Progress userProgress = progressList
                    .stream()
                    .filter(p -> p.getQuestion().equals(question))
                    .findFirst()
                    .orElse(null);

            if (userProgress != null && question.getAnswer().toLowerCase().equals(answer.toLowerCase())) {
                long currentTime = System.currentTimeMillis();
                if (userProgress.getStartTime() + question.getDuration() > currentTime) {
                    userProgress.setSuccessful(true);
                } else {
                    userProgress.setSuccessful(false);
                }
                userProgress.setEndTime(currentTime);
                progressService.save(userProgress);
                return new ResponseEntity<>(HttpStatus.OK);

            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/{quest_id}/{question_number}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getQuestion(
            Authentication authentication,
            @PathVariable Long quest_id,
            @PathVariable Long question_number
    ) {
        question_number = question_number - 1;
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User currentUser = userService.getByEmail(userDetails.getUsername());
        List<Game> userGames = gameService.getGamesByUserId(currentUser.getId());
        Quest quest = questService.getById(quest_id);
        List<Question> questions = quest.getQuestions();
        Question question = questions.get(Math.toIntExact(question_number));
        boolean lastQuestion = false;

        if (question_number + 1 == questions.size()) {
            lastQuestion = true;
        }

        if (userGames.size() == 0 && question_number == 0) { // user have not games

            Game game = gameService.save(Game.builder().user(currentUser).build());
            if (question != null) {
                List<Long> hintsTime = new ArrayList<>();
                question.getHints().forEach(hint -> hintsTime.add(hint.getDuration()));
                Progress progress = Progress.builder()
                        .game(game)
                        .question(question)
                        .startTime(System.currentTimeMillis())
                        .build();
                progressService.save(progress);
                QuestionDto questionDto = QuestionConverter.convertFromEntity(question);
                questionDto.setHintsTime(hintsTime);
                questionDto.setLastQuestion(lastQuestion);
                return new ResponseEntity<>(questionDto, HttpStatus.OK);
            }

        } else {

            Game userGame = getGameByUserAndQuestion(userGames, quest);
            if (userGame != null) {
                List<Progress> progressList = userGame.getProgresses();
                List<Question> showedQuestions = progressList
                        .stream()
                        .map(Progress::getQuestion)
                        .collect(Collectors.toList());

                List<Question> notShowedQuestions = getNotShowedQuestions(questions, showedQuestions);
                if (showedQuestions.contains(question) || question.equals(notShowedQuestions.get(0))) {
                    Progress userProgress = question.getProgresses().stream().findFirst().orElse(null);
                    if (userProgress != null) {
                        if (userProgress.getStartTime() + question.getDuration() < System.currentTimeMillis()) {
                            userProgress.setEndTime(System.currentTimeMillis());
                            userProgress.setSuccessful(false);
                            progressService.save(userProgress);
                            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

                        } else {

                            long currentTime = System.currentTimeMillis();
                            List<Long> hintsTime = new ArrayList<>();
                            question.getHints().forEach(hint -> {
                                if (userProgress.getStartTime() + hint.getDuration() > currentTime) {
                                    hintsTime.add(userProgress.getStartTime() + hint.getDuration() - currentTime);
                                }
                            });
                            QuestionDto questionDto = QuestionConverter.convertFromEntity(question);
                            questionDto.setDuration(userProgress.getStartTime() + question.getDuration() - currentTime);
                            questionDto.setHintsTime(hintsTime);
                            questionDto.setLastQuestion(lastQuestion);
                            return new ResponseEntity<>(questionDto, HttpStatus.OK);
                        }

                    } else {

                        List<Long> hintsTime = new ArrayList<>();
                        question.getHints().forEach(hint -> hintsTime.add(hint.getDuration()));
                        Progress progress = Progress.builder()
                                .game(userGame)
                                .question(question)
                                .startTime(System.currentTimeMillis())
                                .build();
                        progressService.save(progress);
                        QuestionDto questionDto = QuestionConverter.convertFromEntity(question);
                        questionDto.setHintsTime(hintsTime);
                        questionDto.setLastQuestion(lastQuestion);
                        return new ResponseEntity<>(questionDto, HttpStatus.OK);
                    }
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/hint/{quest_id}/{question_number}/{hint_number}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getQuestion(
            Authentication authentication,
            @PathVariable Long quest_id,
            @PathVariable Long question_number,
            @PathVariable Long hint_number
    ) {
        question_number = question_number - 1;
        hint_number = hint_number - 1;
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User currentUser = userService.getByEmail(userDetails.getUsername());
        Quest quest = questService.getById(quest_id);
        List<Question> userQuestions = quest.getQuestions();
        Question question = userQuestions.get(Math.toIntExact(question_number));
        Progress progress = question.getProgresses().stream().findFirst().orElse(null);
        if (progress != null) {
            long currentTime = System.currentTimeMillis();
            List<Hint> hints = question.getHints();
            Hint hint = hints.get(Math.toIntExact(hint_number));
            if (progress.getStartTime() + hint.getDuration() < currentTime) {
                HintDto hintDto = HintConverter.convertFromEntity(hint);
                return new ResponseEntity<>(hintDto, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private Game getGameByUserAndQuestion(List<Game> games, Quest quest) {
        Game userGame = null;
        for (Game game : games) {
            Progress userProgress = game.getProgresses()
                    .stream().filter(progress -> progress.getQuestion().getQuest().getId().equals(quest.getId()))
                    .findFirst().orElse(null);
            if (userProgress != null) {
                userGame = game;
            }
        }
        return userGame;
    }

    private List<Question> getNotShowedQuestions(List<Question> questions, List<Question> questionsShowed) {
        return questions
                .stream()
                .filter(question -> !questionsShowed.contains(question))
                .collect(Collectors.toList());
    }
}



