package ua.ithillel.evo.questengine.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.ithillel.evo.questengine.data.converter.QuestConverter;
import ua.ithillel.evo.questengine.data.converter.RequestQuestsConverter;
import ua.ithillel.evo.questengine.data.dto.QuestDto;
import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;
import ua.ithillel.evo.questengine.models.RequestQuests;
import ua.ithillel.evo.questengine.service.QuestService;
import ua.ithillel.evo.questengine.service.UserService;
import ua.ithillel.evo.questengine.util.JwtUtil;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/quests")
public class QuestController {

    private QuestService questService;
    private UserService userService;
    private JwtUtil jwtUtil;

    @Autowired
    public QuestController(QuestService questService, JwtUtil jwtUtil, UserService userService) {
        this.questService = questService;
        this.jwtUtil = jwtUtil;
        this.userService = userService;
    }

    private Long getUserIdFromToken(String jwt_token) {
        String token = jwt_token.replace("Bearer ", "");
        return Long.parseLong(jwtUtil.extractClaim(token, claim -> claim.get("id")).toString());
    }

    //    open method OPTIONS to endpoint /quests
    @RequestMapping(method = RequestMethod.OPTIONS)
    public ResponseEntity<Void> optionsToQuests(HttpServletResponse response) {
        response.setHeader("Allow", "HEAD,GET,PUT,OPTIONS");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<QuestDto>> getAllByParameters(@RequestParam Map<String, String> requestParam) {
        Set<Quest> quests = new HashSet<>();
        if (requestParam.size() == 0) {
            return new ResponseEntity<>(
                    questService.getAll().stream().map(QuestConverter::convertFromEntity).collect(Collectors.toList()),
                    HttpStatus.OK
            );
        } else {

            for (String key : requestParam.keySet()) {
                if (!requestParam.get(key).equals("")) {
                    String value = requestParam.get(key);
                    if (value.substring(value.length() - 1).equals("?")) {
                        requestParam.put(key, value.substring(0, value.length() - 1));
                    }
                }
            }

            RequestQuests requestQuests = RequestQuestsConverter.convertFromMap(requestParam);

            if (requestQuests.getAuthor() == null || requestQuests.getAuthor().equals("?")) {
                requestQuests.setAuthor("");
            }

            if (requestQuests.getType() == null || requestQuests.getType().equals("?")) {
                requestQuests.setType("");
            }

            if (requestQuests.getSearch() == null || requestQuests.getSearch().equals("?")) {
                requestQuests.setSearch("");
            }
            // filter by type=="" and search=="" and author==""
            if (requestQuests.getType().equals("")
                    && requestQuests.getSearch().equals("")
                    && requestQuests.getAuthor().equals("")) {
                quests.addAll(questService.getAll());
            }
            // filter by type!="" and search=="" and author==""
            if (!requestQuests.getType().equals("")
                    && requestQuests.getSearch().equals("")
                    && requestQuests.getAuthor().equals("")) {
                for (String type : requestQuests.getType().split(",")) {
                    quests.addAll(questService.getQuestsByType(Type.valueOf(type.toUpperCase())));
                }
            }
            // filter by type!="" and search!="" and author!=""
            if (!requestQuests.getType().equals("")
                    && !requestQuests.getSearch().equals("")
                    && !requestQuests.getAuthor().equals("")) {
                for (String type : requestQuests.getType().split(",")) {
                    for (String search : requestQuests.getSearch().split(",")) {
                        quests.addAll(questService.getQuestsByTypeAndUserIdAndNameOrDescription(
                                Type.valueOf(type.toUpperCase()),
                                Long.valueOf(requestQuests.getAuthor()),
                                search, search
                        ));
                    }
                }
            }
            // filter by type!="" and search!="" and author==""
            if (!requestQuests.getType().equals("")
                    && !requestQuests.getSearch().equals("")
                    && requestQuests.getAuthor().equals("")) {
                for (String type : requestQuests.getType().split(",")) {
                    for (String search : requestQuests.getSearch().split(",")) {
                        quests.addAll(questService.getQuestsByTypeAndNameOrDescription(
                                Type.valueOf(type.toUpperCase()), search));
                    }
                }
            }
            // filter by type!="" and search=="" and author!=""
            if (!requestQuests.getType().equals("")
                    && requestQuests.getSearch().equals("")
                    && !requestQuests.getAuthor().equals("")) {
                for (String type : requestQuests.getType().split(",")) {
                    quests.addAll(questService.getQuestsByTypeAndUserId(
                            Type.valueOf(type.toUpperCase()), Long.valueOf(requestQuests.getAuthor())
                    ));
                }
            }
            // filter by type=="" and search!="" and author==""
            if (requestQuests.getType().equals("")
                    && !requestQuests.getSearch().equals("")
                    && requestQuests.getAuthor().equals("")) {
                for (String search : requestQuests.getSearch().split(",")) {
                    quests.addAll(questService.getQuestsByNameOrDescription(search, search));
                }
            }

        }
        return new ResponseEntity<>(
                quests.stream().map(QuestConverter::convertFromEntity).collect(Collectors.toList()),
                HttpStatus.OK
        );
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> create(
            @Valid @RequestBody QuestDto questDto,
            @RequestHeader("Authorization") String jwt_token
    ) {
        Long userId = 0L;
        if (jwt_token != null && jwt_token.startsWith("Bearer")) {
            userId = getUserIdFromToken(jwt_token);
        }
        Quest quest = QuestConverter.convertFromDto(questDto);
        quest.setUser(userService.getById(userId));
        Quest savedQuest = questService.save(quest);
        return new ResponseEntity<>(savedQuest.getId(), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<QuestDto> getById(@PathVariable Long id) {
        Quest quest = questService.getById(id);
        if (quest != null) {
            return new ResponseEntity<>(QuestConverter.convertFromEntity(quest), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/user/{user_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<QuestDto>> getQuestsByUserId(@PathVariable Long user_id) {
        List<Quest> quests = questService.getQuestsByUserId(user_id);
        if (quests.size() > 0) {
            return new ResponseEntity<>(
                    quests.stream().map(QuestConverter::convertFromEntity).collect(Collectors.toList()), HttpStatus.OK
            );
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateQuest(@Valid @RequestBody QuestDto questDto, @PathVariable Long id) {
        Quest quest = questService.getById(id);
        Quest newQuest = QuestConverter.convertFromDto(questDto);
        if (quest != null) {
            quest.setName(newQuest.getName());
            quest.setDescription(newQuest.getDescription());
            quest.setImageLink(questDto.getImageLink());
            quest.setType(newQuest.getType());
            quest.setAccessTime(newQuest.getAccessTime());
            quest.setIsPublic(newQuest.getIsPublic());
            questService.save(quest);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        questService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}