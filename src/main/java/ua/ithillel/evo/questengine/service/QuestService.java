package ua.ithillel.evo.questengine.service;

import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface QuestService {

    Quest getById(Long id);

    List<Quest> getAll();

    List<Quest> getPublic();

    List<Quest> getQuestsByUserId(Long userId);

    List<Quest> getQuestsByTypeAndUserIdAndNameOrDescription(Type type, Long userId, String name, String description);

    List<Quest> getQuestsByTypeAndUserId(Type type, Long userId);

    Set<Quest> getQuestsByTypeAndNameOrDescription(Type type, String searchText);

    List<Quest> getQuestsByType(Type type);

    List<Quest> getQuestsByNameOrDescription(String name, String descriptio);

    Quest save(Quest quest);

    void deleteById(Long id);


}
