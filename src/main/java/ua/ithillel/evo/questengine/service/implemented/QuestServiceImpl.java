package ua.ithillel.evo.questengine.service.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.evo.questengine.data.dao.QuestDAO;
import ua.ithillel.evo.questengine.data.dao.UserDAO;
import ua.ithillel.evo.questengine.data.entity.Quest;
import ua.ithillel.evo.questengine.data.entity.Type;
import ua.ithillel.evo.questengine.service.QuestService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class QuestServiceImpl implements QuestService {

    private QuestDAO questDAO;

    @Autowired
    public QuestServiceImpl(QuestDAO questDAO, UserDAO userDAO) {
        this.questDAO = questDAO;
    }

    @Override
    public Quest getById(Long id) {
        return this.questDAO.getById(id);
    }

    @Override
    public List<Quest> getAll() {
        return this.questDAO.getAll();
    }

    @Override
    public List<Quest> getPublic() {
        return this.questDAO.getPublic();
    }

    @Override
    public List<Quest> getQuestsByUserId(Long userId) {
        return this.questDAO.getQuestsByUserId(userId);
    }

    @Override
    public List<Quest> getQuestsByTypeAndUserIdAndNameOrDescription(Type type, Long userId, String name, String description) {
        return this.questDAO.getQuestsByTypeAndUserIdAndNameOrDescription(
                type, userId, name, description
        );
    }

    @Override
    public List<Quest> getQuestsByTypeAndUserId(Type type, Long userId) {
        return this.questDAO.getQuestsByTypeAndUserId(type, userId);
    }

    @Override
    public Set<Quest> getQuestsByTypeAndNameOrDescription(Type type, String searchText) {
        Set<Quest> quests = new HashSet<>();
        quests.addAll(this.questDAO.getQuestsByTypeAndName(type, searchText));
        quests.addAll(this.questDAO.getQuestsByTypeAndDescription(type, searchText));
        return quests;
    }

    @Override
    public List<Quest> getQuestsByType(Type type) {
        return this.questDAO.getQuestsByType(type);
    }

    @Override
    public List<Quest> getQuestsByNameOrDescription(String name, String description) {
        return this.questDAO.getQuestsByNameOrDescription(name, description);
    }

    @Override
    public Quest save(Quest quest) {
        return this.questDAO.save(quest);
    }

    @Override
    public void deleteById(Long id) {
        this.questDAO.deleteById(id);
    }
}
